/*

App: Booking System API

Scenario:
	A course booking system application whrer a user can enroll into a cource

Type: Course Booking System (Web App)

Description:
	A course booking system application where user can enroll into a course.
	Allows an admin to do CRUD operations.
	Allows users to register into our database.

Features:
	-User Login(user authentication)
	-User Registration

	Customer/Authenticated Users:
	-View Courses (all active courses)
	-Enroll Course

	Admin Users:
	-Add Course
	-Update Course
	-Archive/Unarchive
	-View Courses
	-View/Manage

	All Users(Guest, Customers, Admin)
	-view Active Courses

*/

// Data Model for the Booking System

// Two-way Embedding
/*

user{
	
	id, --unique identifier for the document
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin,
	enrollments:[
		{
		id, --unique identifier for the document
		courseId, --unique identifier for the document
		courseName,
		isPaid,
		dateEnrolled
		}
	]
}



course{

	id, --unique identifier for the document
	name,
	description,
	price,
	slots,
	isActive,
	createdOn,
	enrollees: [
		{
			id, --unique identifier for the document
			userId,
			isPaid,
			dateEnrolled
		}
	]

}

*/



// With Referencing

/*

user{
	
	id, --unique identifier for the document
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin
	}

course{

	id, --unique identifier for the document
	name,
	description,
	price,
	slots,
	isActive,
	createdOn
	}

enrollment{
	
	id, --unique identifier for the document
	userId, - unique identifier for the user
	courseId, - unique identifier for the course
	courseName, - optional
	dateEnrolled
}

*/