const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

//Route for creating a course
//refactor this route to implement user authentication for our admin when creating a course

router.post("/",auth.verify,(req,res)=>{

	const data ={
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController=>res.send(resultFromController))
});

// Route retrieving all the courses
router.get("/all",(req,res)=>{
	courseController.getAllCourses().then(resultFromController=>res.send(resultFromController))
});

// Route for retrieving all the courses
router.get("/",(req,res)=>{
	courseController.getAllActive().then(resultFromController=>res.send(resultFromController))
})

// Route for retrieving a specific course
router.get("/:courseId",(req,res)=>{
	courseController.getCourse(req.params).then(resultFromController=>res.send(resultFromController))
})

// Updating a course
router.put("/:courseId",auth.verify,(req,res)=>{
	courseController.updateCourse(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});

// Activity s40
router.put("/:courseId/archive",auth.verify,(req,res)=>{
	courseController.archiveCourse(req.params, req.body).then(resultFromController=>res.send(resultFromController))
})



// s39 Activity - solution1
// router.post("/",auth.verify,(req,res)=>{

// 	const userAdmin = auth.decode(req.headers.authorization).userAdmin
// 	if(userAdmin == true){
// 		courseController.addCourse(req.body).then(resultFromController=>res.send(resultFromController));
// 	}else{
// 		res.send("Authentication Fail");
// 	}
// })


module.exports = router;