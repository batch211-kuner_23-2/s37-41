const Course = require("../models/Course");
const auth = require("../auth");

//Mini Activity
//Create a new course
/*
	Steps:
	1.Create a new Course object using the mongoose model and the information from the reqBody
		name
		description
		price
	2.Save the new User to the database
	3. Send a screenshot of 3 courses from your database

*/

// Activity s39
// solution2
// module.exports.addCourse = (reqBody) => {

// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	});

// 	if(reqBody.isAdmin){
// 			return newCourse.save().then((course, error) => {

// 		if (error) {

// 			return false;

// 		} else {

// 			return true;

// 		};

// 	});

// 	}else{
// 		return false;
// 	}
// };

// activity solution2
module.exports.addCourse = (data) => {
console.log(data);

	if(data.isAdmin){

		let newCourse = new Course({
		name : data.course.name,
		description : data.course.description,
		price : data.course.price
	});

	console.log(newCourse);
	return newCourse.save().then((course, error) => {

		if (error) {

			return false;

		} else {

			return true;

		};

	});

	}else{
		return false;
	}
};


// Retrieve all courses
/*
1.Retrieve all the courses from the database
	model.find({})
*/
module.exports.getAllCourses = () =>{
	return Course.find({}).then(result=>{
		return result;
	})
}

// Retrieve all Active courses
/*
1.Retrieve all the course from the database with the property of "isActive" to true.
*/

module.exports.getAllActive = () =>{
	return Course.find({isActive:true}).then(result=>{
		return result;
	})
}

// Retrieving a specific course
/*
1.Retrieve the course that matches the course ID provided from the url
*/
module.exports.getCourse = (reqParams)=>{
	return Course.findById(reqParams.courseId).then(result=>{
		return result;
	})
}

// Update a course
/*
1.Create a variable "updatedCourse" which will contain the information retrieved from the request body.
2.Find and update the course using the ID retrieved from the request params property and the variable "updatedCourse"
*/

module.exports.updateCourse = (reqParams,reqBody) =>{
	let updatedCourse ={
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price
	};
	return Course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
};


// Activity s40
module.exports.archiveCourse = (reqParams, reqBody)=>{
	let courseStatus ={
		isActive: reqBody.isActive
	}
	return Course.findByIdAndUpdate(reqParams.courseId,courseStatus).then((course,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
}