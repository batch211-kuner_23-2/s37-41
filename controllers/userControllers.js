const User = require("../models/User");
// bcrypt is a package which allows us to hash our passwords
const bcrypt = require("bcrypt");

const auth = require("../auth");

const Course = require("../models/Course")

const courseController = require("../controllers/courseControllers")
// check if the email already exists
/*
Steps:
1. use mongoose "find" method to find duplicate emails
2. use the ".then" method
*/

module.exports.checkEmailExists = (reqBody) =>{
	return User.find({email:reqBody.email}).then( result =>{
		if(result.length>0){
			return true;
		}else{
			return false;
		};
	});
};


// User registration
/*
Steps:
	1.Create a new User object using mongoose models and the info from the reqBody
*/

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
};

// User authentication
/*
Steps:
	1.chek the database if the user email exists
	2.compare the password in the login form with the password stored in the database
	3.
*/

module.exports.loginUser = (reqBody) =>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result==null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else{
				return false
			};
		};
	});
};

// // Activity 37-41 User details

// module.exports.detailsUser = (data) =>{
// 	return User.findById(data.id).then(result=>{
// 		result.password = "";
// 		return result;
// 	});
// };

module.exports.getProfile = (data) =>{
	return User.findById(data.userId).then(result=>{
		result.password = "";
		return result;
	})
}

// Enroll user to a class
/*
	Steps:
	1.Find the document in the database using the user's ID
	2.Add the courseID to the user enrollment array
	3.Update the document in mongoDB

	
	First, find the user who is enrolling and update his enrollments subdocument array. We will push the courseId in the enrollments array.

	Second, find the course where we are enrolling and update its enrollees subdocument array. We will push the userId in the enrollees array.

	Since we will access 2 collections in one action -- we will have to wait for the completion of the action instead of letting javascript cont. line per line.
*/

module.exports.enroll = async (data)=>{

	let isUserUpdated = await User.findById(data.userId).then(user=>{

		user.enrollments.push({courseId:data.courseId});

		return user.save().then((user,error)=>{
			if(error){
				return false
			}else{
				return true;
			};
		});
	});

	let isCourseUpdated = Course.findById(data.courseId).then(course=>{

		course.enrollees.push({userId:data.userId})

		return course.save().then((course,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
	});

	if(isUserUpdated&&isCourseUpdated){
		return true;
	}else{
		return false;
	};
};